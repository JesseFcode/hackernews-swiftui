//
//  PostData.swift
//  HackerNews SwiftUI
//
//  Created by Jesse Frederick on 2/23/23.
//

import Foundation

struct Results: Decodable {
    let hits: [Post]
}

struct Post: Decodable, Identifiable {
    var id: String {
        return objectID
        }
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
