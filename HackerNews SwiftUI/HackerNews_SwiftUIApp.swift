//
//  HackerNews_SwiftUIApp.swift
//  HackerNews SwiftUI
//
//  Created by Jesse Frederick on 2/23/23.
//

import SwiftUI

@main
struct HackerNews_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
